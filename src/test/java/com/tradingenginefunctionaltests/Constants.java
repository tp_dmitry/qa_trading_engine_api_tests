package com.tradingenginefunctionaltests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constants {
    public static String baseURL;
    public static String orderRequestTemplate;
    public static String orderAverageRequestTemplate;
    public static String updateBookTemplate;
    public static String getPositionTemplate;

    public static void setUp (String path) {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(System.getProperty("user.dir") + path);
            if(input==null){
                System.out.println("Sorry, unable to find " + path);
                return;
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            //get the property value and print it out
            baseURL = prop.getProperty("baseURL");
            orderRequestTemplate = prop.getProperty("orderRequestTemplate");
            orderAverageRequestTemplate = prop.getProperty("orderAverageRequestTemplate");
            updateBookTemplate = prop.getProperty("updateBookTemplate");
            getPositionTemplate = prop.getProperty("getPositionTemplate");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
