package com.tradingenginefunctionaltests.Entities;

import com.tradingenginefunctionaltests.Constants;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class Book {

    private long [][][] book;

    public Book () {
        book = new long[][][] {};
    }

    public Book (long [][][] book) {
        this.book = book;
    }

    public long [][][] getBook() {
        // GET request there
        // Currently not implemented from DEV side so that currently returning in-memory state
        return (this.book);
    }

    public void setBook (long [][][] book) {
            given().
                    contentType(ContentType.JSON).
                    and().
                    body(book).
            when().
                    post(String.format(Constants.updateBookTemplate, Constants.baseURL)).
            then().
                    statusCode(200);
        // Just a stub to cover the missing functionality until we'll get it normally from the pricing engine and / or mock
        this.book = book;
    }

    @Override
    public String toString(){
        String myString = "\nCurrent book:\nBid volume\tBid rate\tLP ID\tAsk volume\tAsk rate\tLP ID\n";
        try {
            for (int j = 0; j < book[0].length; j++) {
                for (int i = 0; i < book.length; i++) {
                    try {
                        for (int k = 0; k < book[i][j].length; k++) {
                            myString += book[i][j][k] + "\t";
                        }
                    }
                    catch (IndexOutOfBoundsException e) {
                        myString += ".\t.\t.\t";
                    }
                    myString += "\t\t\t";
                }
                myString += "\n";
            }
        }
        catch (IndexOutOfBoundsException e) {
            myString = myString + " Book is empty!";
        }
        return (myString);
    }
}
