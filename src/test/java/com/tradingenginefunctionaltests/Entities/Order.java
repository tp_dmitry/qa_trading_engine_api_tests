package com.tradingenginefunctionaltests.Entities;

import com.tradingenginefunctionaltests.Constants;
import io.restassured.http.ContentType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;


import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class Order {

    public static enum OrderDirections {
        BUY ((byte)0),
        SELL ((byte)1);

        private byte direction;

        private OrderDirections(byte direction) {
            this.direction = direction;
        }

        public byte getDirection(){
            return (direction);
        }
    }

    public static enum OrderTypes {
        IOC (0);

        private int orderType;

        OrderTypes(int orderType) {
            this.orderType = orderType;
        }

        public int getOrderType() {
            return (orderType);
        }
    }

    public static final byte BUY = 0;
    public static final byte SELL = 1;

    private long id;
    private int orderType;
    private byte operationType;
    private long volume;
    private long startTime;
    private long[][][] snapshot;

    public Order (OrderTypes orderType, OrderDirections operationType, long volume) {
        Random random = new Random();
        this.id = random.nextInt(1000);
        this.orderType = orderType.getOrderType();
        this.operationType = operationType.getDirection();
        this.volume = volume;
        this.startTime = System.currentTimeMillis();
    }

    public void updateValues(long id, int orderType, byte operationType, long volume, long[][][] book) {
        this.id = id;
        this.orderType = orderType;
        this.operationType = operationType;
        this.volume = volume;
        snapshot = null;
        this.startTime = System.nanoTime();
    }

    public long getVolume() {
        return volume;
    }

    public byte getOperationType() {
        return operationType;
    }

    public int getOrderType() {
        return orderType;
    }

    public long getId() {
        return id;
    }

    public long getStartTime() {
        return startTime;
    }

    public long[][][] getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(long[][][] snapshot) {
        this.snapshot = snapshot;
    }

    @Override
    public String toString () {
        DateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getDefault());
        String dateFormatted = formatter.format(this.startTime);
        String orderPrintTemplate = "\nOrder data: id: %d orderType: %d operationType %d volume: %d time: %s";
        return (String.format(orderPrintTemplate, this.id, this.orderType, this.operationType, this.volume, dateFormatted));
    }

    public void pushOrder() {
        given().
                contentType(ContentType.JSON).
                when().
                post(String.format(Constants.orderRequestTemplate, Constants.baseURL, id, operationType, orderType, volume)).
                then().statusCode(200);
    }
}
