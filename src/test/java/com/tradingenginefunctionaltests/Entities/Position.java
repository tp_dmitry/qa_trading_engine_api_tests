package com.tradingenginefunctionaltests.Entities;

import com.tradingenginefunctionaltests.Constants;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class Position {

    private long orderId;
    private byte type;
    private long volume;
    private long fullPrice;
    private long orderStartTime;
    private long[][] deals;

    public Position() {

    }

    public Position (long orderId, byte type, long volume, long fullPrice, long orderStartTime, long[][] deals) {
        this.orderId = orderId;
        this.type = type;
        this.volume = volume;
        this.fullPrice = fullPrice;
        this.orderStartTime = orderStartTime;
        this.deals = deals;
    }

    public void updateValues(long orderId, byte type, long volume, long startTime, long fullPrice, long[][] deals) {
        this.orderId = orderId;
        this.type = type;
        this.volume = volume;
        this.fullPrice = fullPrice;
        this.deals = deals;
        this.orderStartTime = startTime;
    }

    public void updateValues(Order order, long price, long[][] deals) {
        updateValues(order.getId(), order.getOperationType(), order.getVolume(), order.getStartTime(), price, deals);
    }

    public long getOrderId() {
        return orderId;
    }

    public byte getType() {
        return type;
    }

    public long getVolume() {
        return volume;
    }

    public long getFullPrice() {
        return fullPrice;
    }

    public long getOrderStartTime() {
        return orderStartTime;
    }

    public void setOrderStartTime(long orderStartTime) {
        this.orderStartTime = orderStartTime;
    }

    public long[][] getDeals() {
        return deals;
    }

    @Override
    public String toString() {
        String positionFormatTemplate = "\nPosition details: orderId: %d type: %d volume: %d fullPrice: %d orderStartTime: %d %s";

        // Deals formatting is not implemented yet
        String deals = "";

        return (String.format(positionFormatTemplate, orderId, type, volume, fullPrice, orderStartTime, deals));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Position pos = (Position)obj;
        boolean dealMatch = true;
        for (int i = 0; i < deals.length; i++) {
            for (int j = 0; j < deals[i].length; j++) {
                dealMatch &= (dealMatch && deals[i][j] == pos.deals [i][j]);
            }
        }
        return (dealMatch &&
                orderId == pos.orderId &&
                type == pos.type &&
                volume == pos.volume &&
                fullPrice == pos.fullPrice
                // Currently orderStartTime is time when order comes to the processing,
                // it is not the time when order was created on UI
                // So that orderStartTime will not be the same for the order & for position.
                /*&& orderStartTime == pos.orderStartTime*/
        );

    }

    public static Position getPosition (long orderId) {
        return (
                given().contentType(ContentType.JSON).
                when().delete(String.format(Constants.getPositionTemplate, Constants.baseURL, orderId)).
                then().statusCode(200).extract().response().as(Position.class)
        );
    }
}