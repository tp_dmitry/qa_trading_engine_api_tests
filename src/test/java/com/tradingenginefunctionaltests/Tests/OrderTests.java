package com.tradingenginefunctionaltests.Tests;

import com.tradingenginefunctionaltests.Constants;
import com.tradingenginefunctionaltests.Entities.Order;
import com.tradingenginefunctionaltests.Entities.Position;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import com.tradingenginefunctionaltests.Entities.Book;


@Test (groups = { "Functional" } )
public class OrderTests {
    final Logger logger = LogManager.getLogger(OrderTests.class);

    @Parameters ( { "Path to config file"} )
    @BeforeMethod
    public void setUp( String path) {
        Constants.setUp(path);
    }

    @Test(testName = "Order Tests", groups = { "Functional", "Sprint Regression", "Full Regression" },
            dataProvider = "DP_For_Market_IOC_ORDERS")
    public void MarketOrderTest(long [][][] testBook, Order testOrder, Position expectedPosition) {
        Book book = new Book(testBook);
        // Sending book to server
        // As far as there is no mock now, I have to send the book manually
        book.setBook(book.getBook());
        logger.info(book);
        // Sending order
        testOrder.pushOrder();
        logger.info(testOrder);
        // Take calculated position
        Position position = Position.getPosition(testOrder.getId());
        logger.info(position);
        logger.info(expectedPosition);
        // Verify position is correct
        assert (position.equals(expectedPosition));
        // Verify the snapshots are correct
        // TODO: not implemented yet from dev side. Nothing to verify now.
    }

    @DataProvider (name = "DP_For_Market_IOC_ORDERS")
    public Object [][] getTestData() {
        long [][][] testBook14Buy = new long[][][]{
                new long[][]{
                        new long[]{100000, 120000, 1},
                        new long[]{500000, 110000, 1},
                        new long[]{200000, 100000, 1},
                        new long[]{400000, 90000, 1},
                }
                ,
                new long[][]{
                        new long[]{100000, 130000, 1},
                        new long[]{500000, 140000, 1},
                        new long[]{200000, 150000, 1},
                        new long[]{400000, 160000, 1}
                }
        };
        long [][][] testBook5Buy = new long[][][]{
                new long[][]{
                        new long[]{100000, 120000, 1},
                        new long[]{500000, 110000, 1},
                        new long[]{200000, 100000, 1},
                        new long[]{400000, 90000, 1},
                }
                ,
                new long[][]{
                }
        };
        long [][][] testBook6Buy = new long[][][]{
                new long[][]{
                }
                ,
                new long[][]{
                        new long[]{100000, 130000, 1},
                        new long[]{500000, 140000, 1},
                        new long[]{200000, 150000, 1},
                        new long[]{400000, 160000, 1}
                }
        };
        long [][][] testBook7Buy = new long[][][]{
                new long[][]{
                        new long[]{100000, 120000, 1},
                        new long[]{500000, 110000, 1},
                        new long[]{200000, 100000, 1},
                        new long[]{400000, 90000, 1},
                }
                ,
                new long[][]{
                        new long[]{100000, 130000, 1},
                }
        };
        long [][][] testBook8Buy = new long[][][]{
                new long[][]{
                }
                ,
                new long[][]{
                }
        };

        long [][][][] testBooks = new long [][][][] {
                testBook14Buy,
                testBook14Buy,
                testBook14Buy,
                testBook14Buy,
                testBook5Buy,
                testBook6Buy,
                testBook7Buy,
                testBook8Buy,
                testBook14Buy,
                testBook14Buy,
                testBook14Buy,
                testBook14Buy,
                testBook5Buy,
                testBook6Buy,
                testBook7Buy,
                testBook8Buy
        };


        Order [] orders = new Order[16];
        Position [] expectedPositions = new Position[16];
        long [] buyVolumes = new long []{100000, 300000, 1200000, 2000000, 1000, 1000, 200000, 1000};
        long [] sellVolumes = new long []{100000, 300000, 1200000, 2000000, 1000, 1000, 200000, 1000};
        long [] expectedBuyFullPrices = new long [] {13000000000l, 41000000000l, 177000000000l, 177000000000l, 0, 130000000l, 12000000000l, 0};
        long [] expectedSellFullPrices = new long [] {12000000000l, 34000000000l, 123000000000l, 123000000000l, 120000000l, 0, 12000000000l, 0};

        // BUY orders data
        for (int i = 0; i < 8; i++) {
            orders[i] = new Order(Order.OrderTypes.IOC, Order.OrderDirections.BUY, sellVolumes[i]);
            long[][] deals = new long[][]{new long[]{orders[i].getVolume(), 1}};
            expectedPositions[i] = new Position(
                    orders[i].getId(),
                    orders[i].getOperationType(),
                    orders[i].getVolume(),
                    expectedBuyFullPrices[i],
                    orders[i].getStartTime(),
                    deals);
        }

        // SELL orders data
        for (int i = 0; i < 8; i++) {
            orders[i + 8] = new Order(Order.OrderTypes.IOC, Order.OrderDirections.SELL, buyVolumes[i]);
            long[][] deals = new long[][]{new long[]{orders[i + 8].getVolume(), 1}};
            expectedPositions[i + 8] = new Position(
                    orders[i + 8].getId(),
                    orders[i + 8].getOperationType(),
                    orders[i + 8].getVolume(),
                    expectedSellFullPrices[i],
                    orders[i + 8].getStartTime(),
                    deals);
        }

        Object [][] testData = new Object[16][3];
        for (int i = 0; i < 16; i++) {
            testData[i][0] = testBooks[i];
            testData[i][1] = orders[i];
            testData[i][2] = expectedPositions[i];
        }
        return testData;
    }
}
